package br.com.projetohtmlunit;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomText;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableBody;
import com.gargoylesoftware.htmlunit.html.HtmlTableDataCell;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

/**
 * Hello world!
 *
 */
public class App 
{
	private static HtmlPage page;
	private static WebClient webClient;
	
    public static void main( String[] args )
    {
    	printMs("1018104160053");
    	printMs("1036700760284");
    	printMs("1130001830167");
    	webClient.close();
    }
    
    public static void setPage(Object pagina) {
    	page = (HtmlPage) pagina;
    }
    	
    public static HtmlPage getPage() {
    	if(page == null) {
    		webClient = new WebClient();
    		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			webClient.getOptions().setJavaScriptEnabled(false);
			try {
				page = webClient.getPage("http://sngpc.anvisa.gov.br/ConsultaMedicamento/index.asp");
			} catch (Exception e) {
				 throw new RuntimeException("Erro ao obter a página: "+e.getMessage());
			}
    	}
    	return page;
    }
    
    public static String getDomText(HtmlTableRow linha) {
    	HtmlTableDataCell table = (HtmlTableDataCell) linha.getChildNodes().get(0);
    	for(Object obj : table.getChildNodes()) {
    		if(obj instanceof DomText) {
    			return ((DomText) obj).getData();
    		}
    	}
    	return "";
    }
    
    public static void printMs(String ms) {
    	try {
			HtmlForm form = getPage().getFormByName("frmInventario");
			HtmlTable table = getPage().getHtmlElementById("Table1");
	        HtmlButton button = (HtmlButton)table.getCellAt(2,0).getChildNodes().get(3);
	        HtmlTextInput textField = form.getInputByName("NU_REG");
	        textField.setValueAttribute(ms);
	        setPage(button.click());
	        table = getPage().getHtmlElementById("Table1");
	        HtmlTableBody body = (HtmlTableBody)table.getChildNodes().get(0);
			System.out.println("MS-"+ms+"=" + getDomText((HtmlTableRow)body.getChildNodes().get(3)).toString().trim());
			System.out.println("MS-"+ms+"=" + getDomText((HtmlTableRow)body.getChildNodes().get(4)).toString().trim());
			System.out.println("MS-"+ms+"=" + getDomText((HtmlTableRow)body.getChildNodes().get(5)).toString().trim());
			System.out.println("MS-"+ms+"=" + getDomText((HtmlTableRow)body.getChildNodes().get(6)).toString().trim());
			System.out.println("---------------------------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
